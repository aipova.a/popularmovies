package com.aipova.popularmovies

import org.junit.Assert.assertEquals
import org.junit.Test

class MyInteractorTest {
    val subject = MyInteractor()

    @Test
    fun testMyCoverageFun() {
        assertEquals("atest", subject.testMyCoverageFun("a"))
    }
}
