build:
  maxIssues: 0
  weights:
    # complexity: 2
    # LongParameterList: 1
    # style: 1
    # comments: 1

processors:
  active: true
  exclude:
  # - 'FunctionCountProcessor'
  # - 'PropertyCountProcessor'
  # - 'ClassCountProcessor'
  # - 'PackageCountProcessor'
  # - 'KtFileCountProcessor'

console-reports:
  active: true
  exclude:
  #  - 'ProjectStatisticsReport'
  #  - 'ComplexityReport'
  #  - 'NotificationReport'
  #  - 'FindingsReport'
  #  - 'BuildFailureReport'

comments:
  active: true
  CommentOverPrivateFunction:
    active: false
  CommentOverPrivateProperty:
    active: false
  EndOfSentenceFormat:
    active: false
    endOfSentenceFormat: ([.?!][ \t\n\r\f<])|([.?!:]$)
  UndocumentedPublicClass:
    active: false
    searchInNestedClass: true
    searchInInnerClass: true
    searchInInnerObject: true
    searchInInnerInterface: true
  UndocumentedPublicFunction:
    active: false
  UndocumentedPublicProperty:
    active: false

complexity:
  active: true
  ComplexCondition:
    active: true
    threshold: 4
  ComplexInterface:
    active: false
    threshold: 10
    includeStaticDeclarations: false
  ComplexMethod:
    active: true
    threshold: 30
    ignoreSingleWhenExpression: true
  LabeledExpression:
    active: false
  LargeClass:
    active: true
    threshold: 800
  LongMethod:
    active: true
    threshold: 80
  LongParameterList:
    active: true
    threshold: 7
    ignoreDefaultParameters: false
  MethodOverloading:
    active: false
    threshold: 6
  NestedBlockDepth:
    active: true
    threshold: 4
  StringLiteralDuplication:
    active: false
    threshold: 3
    ignoreAnnotation: true
    excludeStringsWithLessThan5Characters: true
    ignoreStringsRegex: '$^'
  TooManyFunctions:
    active: true
    thresholdInFiles: 30
    thresholdInClasses: 40
    thresholdInInterfaces: 20
    thresholdInObjects: 30
    thresholdInEnums: 15

empty-blocks:
  active: true
  EmptyCatchBlock:
    active: true
    allowedExceptionNameRegex: "^(_|(ignore|expected).*)"
  EmptyClassBlock:
    active: true
  EmptyDefaultConstructor:
    active: true
  EmptyDoWhileBlock:
    active: true
  EmptyElseBlock:
    active: true
  EmptyFinallyBlock:
    active: true
  EmptyForBlock:
    active: true
  EmptyFunctionBlock:
    active: true
  EmptyIfBlock:
    active: true
  EmptyInitBlock:
    active: true
  EmptyKtFile:
    active: true
  EmptySecondaryConstructor:
    active: true
  EmptyTryBlock:
    active: true
  EmptyWhenBlock:
    active: true
  EmptyWhileBlock:
    active: true

exceptions:
  active: true
  ExceptionRaisedInUnexpectedLocation:
    active: false
    methodNames: 'toString,hashCode,equals,finalize'
  InstanceOfCheckForException:
    active: false
  NotImplementedDeclaration:
    active: false
  PrintStackTrace:
    active: false
  RethrowCaughtException:
    active: false
  ReturnFromFinally:
    active: false
  SwallowedException:
    active: false
  ThrowingExceptionFromFinally:
    active: false
  ThrowingExceptionInMain:
    active: false
  ThrowingExceptionsWithoutMessageOrCause:
    active: false
    exceptions: 'IllegalArgumentException,IllegalStateException,IOException'
  ThrowingNewInstanceOfSameException:
    active: false
  TooGenericExceptionCaught:
    active: true
    exceptionNames:
     - ArrayIndexOutOfBoundsException
     - Error
     - Exception
     - IllegalMonitorStateException
     - NullPointerException
     - IndexOutOfBoundsException
     - RuntimeException
     - Throwable
  TooGenericExceptionThrown:
    active: true
    exceptionNames:
     - Error
     - Exception
     - Throwable
     - RuntimeException

naming:
  active: true
  ClassNaming:
    active: true
    classPattern: '[A-Z$][a-zA-Z0-9$]*'
  EnumNaming:
    active: true
    enumEntryPattern: '^[A-Z][_a-zA-Z0-9]*'
  ForbiddenClassName:
    active: false
    forbiddenName: ''
  FunctionMaxLength:
    active: false
    maximumFunctionNameLength: 30
  FunctionMinLength:
    active: false
    minimumFunctionNameLength: 3
  FunctionNaming:
    active: true
    functionPattern: '^([a-z$][a-zA-Z$0-9]*)|(`.*`)$'
    excludeClassPattern: '$^'
    ignoreOverridden: true
  FunctionParameterNaming:
    active: true
    parameterPattern: '[a-z][A-Za-z0-9]*'
    excludeClassPattern: '$^'
    ignoreOverridden: true
  MatchingDeclarationName:
    active: true
    mustBeFirst: true
  MemberNameEqualsClassName:
    active: false
  ObjectPropertyNaming:
    active: true
    propertyPattern: '[A-Za-z][_A-Za-z0-9]*'
  PackageNaming:
    active: true
    packagePattern: '^[a-z]+(\.[a-z][a-z0-9]*)*$'
  TopLevelPropertyNaming:
    active: false
    constantPattern: '[A-Z][_A-Z0-9]*'
    propertyPattern: '[a-z][A-Za-z\d]*'
    privatePropertyPattern: '(_)?[a-z][A-Za-z0-9]*'
  VariableMaxLength:
    active: false
    maximumVariableNameLength: 64
  VariableMinLength:
    active: false
    minimumVariableNameLength: 1
  VariableNaming:
    active: true
    variablePattern: '[a-z][A-Za-z0-9]*'
    privateVariablePattern: '(_)?[a-z][A-Za-z0-9]*'
    excludeClassPattern: '$^'

performance:
  active: true
  ArrayPrimitive:
    active: true
  ForEachOnRange:
    active: true
  SpreadOperator:
    active: false
  UnnecessaryTemporaryInstantiation:
    active: true

potential-bugs:
  active: true
  Deprecation:
    active: false
  DuplicateCaseInWhenExpression:
    active: true
  EqualsAlwaysReturnsTrueOrFalse:
    active: true
  EqualsWithHashCodeExist:
    active: true
  ExplicitGarbageCollectionCall:
    active: true
  HasPlatformType:
    active: false
  ImplicitDefaultLocale:
    active: false
  InvalidRange:
    active: true
  IteratorHasNextCallsNextMethod:
    active: true
  IteratorNotThrowingNoSuchElementException:
    active: true
  LateinitUsage:
    active: false
    excludes: "**/test/**,**/androidTest/**,**/*.Test.kt,**/*.Spec.kt,**/*.Spek.kt"
    excludeAnnotatedProperties: ""
    ignoreOnClassesPattern: ""
  MapGetWithNotNullAssertionOperator:
    active: false
  MissingWhenCase:
    active: true
  RedundantElseInWhen:
    active: true
  UnconditionalJumpStatementInLoop:
    active: false
  UnreachableCode:
    active: true
  UnsafeCallOnNullableType:
    active: true
  UnsafeCast:
    active: false
  UselessPostfixExpression:
    active: false
  WrongEqualsTypeParameter:
    active: true

style:
  active: true
  CollapsibleIfStatements:
    active: false
  DataClassContainsFunctions:
    active: false
    conversionFunctionPrefix: 'to'
  DataClassShouldBeImmutable:
    active: false
  EqualsNullCall:
    active: true
  EqualsOnSignatureLine:
    active: false
  ExplicitCollectionElementAccessMethod:
    active: false
  ExplicitItLambdaParameter:
    active: false
  ExpressionBodySyntax:
    active: false
  ForbiddenComment:
    active: false
    values: 'TODO:,FIXME:,STOPSHIP:'
  ForbiddenImport:
    active: false
    imports: ''
    forbiddenPatterns: ""
  ForbiddenMethodCall:
    active: false
    methods: ''
  ForbiddenPublicDataClass:
    active: false
    ignorePackages: '*.internal,*.internal.*'
  ForbiddenVoid:
    active: false
    ignoreOverridden: false
    ignoreUsageInGenerics: false
  FunctionOnlyReturningConstant:
    active: true
    ignoreOverridableFunction: true
    excludedFunctions: 'describeContents'
    excludeAnnotatedFunction: "dagger.Provides"
  LibraryCodeMustSpecifyReturnType:
    active: true
  LoopWithTooManyJumpStatements:
    active: false
    maxJumpCount: 1
  MagicNumber:
    active: true
    ignoreNumbers: '-1,0,1,2'
    ignoreHashCodeFunction: true
    ignorePropertyDeclaration: false
    ignoreLocalVariableDeclaration: false
    ignoreConstantDeclaration: true
    ignoreCompanionObjectPropertyDeclaration: true
    ignoreAnnotation: false
    ignoreNamedArgument: true
    ignoreEnums: false
    ignoreRanges: false
  MandatoryBracesIfStatements:
    active: false
  MaxLineLength:
    active: true
    maxLineLength: 150
    excludePackageStatements: false
    excludeImportStatements: false
  MayBeConst:
    active: true
  ModifierOrder:
    active: true
  NestedClassesVisibility:
    active: false
  NewLineAtEndOfFile:
    active: false
  NoTabs:
    active: false
  OptionalAbstractKeyword:
    active: true
  OptionalUnit:
    active: false
  OptionalWhenBraces:
    active: false
  PreferToOverPairSyntax:
    active: false
  ProtectedMemberInFinalClass:
    active: true
  RedundantExplicitType:
    active: false
  RedundantVisibilityModifierRule:
    active: false
  ReturnCount:
    active: false
    max: 3
    excludedFunctions: "equals"
  SafeCast:
    active: true
  SerialVersionUIDInSerializableClass:
    active: false
  SpacingBetweenPackageAndImports:
    active: false
  ThrowsCount:
    active: true
    max: 2
  TrailingWhitespace:
    active: false
  UnderscoresInNumericLiterals:
    active: false
    acceptableDecimalLength: 5
  UnnecessaryAbstractClass:
    active: false
  UnnecessaryInheritance:
    active: true
  UnnecessaryLet:
    active: false
  UnnecessaryParentheses:
    active: false
  UntilInsteadOfRangeTo:
    active: false
  UnusedImports:
    active: false
  UnusedPrivateClass:
    active: true
  UnusedPrivateMember:
    active: false
    allowedNames: "(_|ignored|expected|serialVersionUID)"
  UseArrayLiteralsInAnnotations:
    active: false
  UseCheckOrError:
    active: false
  UseDataClass:
    active: false
    excludeAnnotatedClasses: ""
    allowVars: false
  UseIfInsteadOfWhen:
    active: false
  UseRequire:
    active: false
  UselessCallOnNotNull:
    active: true
  UtilityClassWithPublicConstructor:
    active: true
  VarCouldBeVal:
    active: false
  WildcardImport:
    active: true
    excludeImports: 'java.util.*,kotlinx.android.synthetic.*'